<?php

require_once './config.php';

//////////////////////////////////////////////////////////////////////
// INIT SLIM
//////////////////////////////////////////////////////////////////////
$config = array(
    'templates.path' => './app/resources/templates'
);
$app = new \Slim\Slim($config);

$app->hook('slim.before', function () use ($app) {
    $app->view()->appendData(array('baseUrl' => ROOT_PATH));
});

$app->view(new \Slim\Views\Twig());

$app->view->parserOptions = array(
    'charset' => 'utf-8',
    'cache' => realpath('./cache'),
    'auto_reload' => true,
    'strict_variables' => false,
    'autoescape' => true
);

$app->view->parserExtensions = array(new \Slim\Views\TwigExtension());

//////////////////////////////////////////////////////////////////////
// AUTHENTICATION
//////////////////////////////////////////////////////////////////////
$headers = apache_request_headers();
//print_r($headers);
$authDAO = new AuthDAO();

$app->add(new OAuth2Auth($headers, $authDAO));

$app->notFound(function () use ($app) {
    $app->response()->header('Content-Type', 'application/json;charset=utf-8');
    $app->response()->status(404);
    $errorArray = ErrorCodeMapping::$VALID_METHOD_NOT_FOUND;
    if ($errorArray && count($errorArray) == 2) {
        $app->response()->header(ERROR_CODE_TAG, $errorArray[0]);
        $app->response()->header(ERROR_MESSAGE_TAG, $errorArray[1]);
    }
});

//////////////////////////////////////////////////////////////////////
// CONFIG FOR SLIM
//////////////////////////////////////////////////////////////////////
$mode = DEVELOPMENT_MODE == TRUE ? "development" : "production";
$app->config("mode", $mode);

if (isset($headers[HEADER_UID])) {
    $requesterId = $headers[HEADER_UID];
} else {
    $requesterId = NULL;
}

//////////////////////////////////////////////////////////////////////
// INIT ENPOINT
//////////////////////////////////////////////////////////////////////
$endPoint = new APIEndpoint($app, $requesterId);

//////////////////////////////////////////////////////////////////////
// DEFINE WS METHODS
//////////////////////////////////////////////////////////////////////

$app->post('/users/auth/social', 'socialAuth');

function socialAuth() {
    global $endPoint;
    $endPoint->socialAuth();
}

// run the Slim app
$app->run();
