<?php

class NMEndPoint {

    protected $app;
    protected $requesterId;

    public function __construct($app, $requesterId) {
        $this->app = $app;
        $this->requesterId = $requesterId;
    }

    protected function returnError($errorArray, $statusCode = 409) {
        $this->app->response()->header('Content-Type', 'application/json');
        $this->app->response()->status($statusCode);
        if ($errorArray && count($errorArray) == 2) {
            $this->addAuthHeader($errorArray[0], $errorArray[1]);
        }
    }

    protected function returnPDOException($ex) {
        $this->app->response()->header('Content-Type', 'application/json');
        $this->app->response()->status(409);
        if ($ex != NULL) {
            $this->addAuthHeader($ex->getCode(), $ex->getMessage());
        }
    }

    protected function returnData($data, $statusCode = 200) {

        $this->app->response()->header('Content-Type', 'application/json');
        $this->app->response()->status($statusCode);
        if ($data != NULL) {
            echo json_encode($data);
        }
    }

    protected function returnInsertedData($row, $statusCode = 201) {
        $this->app->response()->header('Content-Type', 'application/json');
        $this->app->response()->status($statusCode);
        $array = array();
        if (isset($row["id"])) {
            $array["id"] = $row["id"];
        }
        if (isset($row["created"])) {
            $array["created"] = $row["created"];
        }
        echo json_encode($array);
    }

    protected function returnUpdatedData($row, $statusCode = 200) {
        $this->app->response()->header('Content-Type', 'application/json');
        $this->app->response()->status($statusCode);
        $array = array();
        if (isset($row["id"])) {
            $array["id"] = $row["id"];
        }
        if (isset($row["created"])) {
            $array["created"] = $row["created"];
        }
        if (isset($row["modified"])) {
            $array["modified"] = $row["modified"];
        }
        echo json_encode($array);
    }

    protected function returnDeletedData($id, $statusCode = 200) {
        $this->app->response()->header('Content-Type', 'application/json');
        $this->app->response()->status($statusCode);
        echo json_encode(array("id" => $id));
    }

    protected function returnUploadedData($url, $statusCode = 201) {
        $this->app->response()->header('Content-Type', 'application/json');
        $this->app->response()->status($statusCode);
        $array = array();
        if ($url) {
            $array["url"] = $url;
        }

        echo json_encode($array);
    }

    protected function returnSuccess($statusCode = 200) {
        $this->app->response()->header('Content-Type', 'application/json');
        $this->app->response()->status($statusCode);
    }

    protected function addAuthHeader($code, $message) {
        $this->app->response()->header(ERROR_CODE_TAG, $code);
        $this->app->response()->header(ERROR_MESSAGE_TAG, $message);
    }

    protected function processRequestBody() {
        $body = $this->app->request()->getBody();
        $json_data = json_decode($body, true);
        if ($json_data === NULL && $json_data !== JSON_ERROR_NONE) {
            $this->returnError(ErrorCodeMapping::$VALID_REQUEST_BAD);
            return NULL;
        }
        return $json_data;
    }

    protected function returnValidateError($errorArray) {
        $this->app->response()->header('Content-Type', 'application/json;charset=utf-8');
        $this->app->response()->status(406);
        if ($errorArray && count($errorArray) == 2) {
            $this->app->response()->header(ERROR_CODE_TAG, $errorArray[0]);
            $this->app->response()->header(ERROR_MESSAGE_TAG, $errorArray[1]);
        }
    }

    protected function validPermission($id) {
        if (!isset($this->requesterId) || $id != $this->requesterId) {
            $this->returnError(ErrorCodeMapping::$AUTH_DENIED, 401);
            return FALSE;
        }
        return TRUE;
    }

}