<?php

/**
 * Description of NMDAO
 *
 * @author Linh NGUYEN
 */
class NMDAO {

    private $sHostname;
    private $sUsername;
    private $sPassword;
    private $sDatabase;
    protected $pdo;
    protected $db;

    // contructor
    public function __construct($host, $user, $pass, $db) {
        $this->sHostname = $host;
        $this->sUsername = $user;
        $this->sPassword = $pass;
        $this->sDatabase = $db;
        $this->connect();
    }

    // init connection
    private function connect() {
        if ($this->pdo) {
            mysql_close($this->pdo);
        }
        try {
            $this->pdo = new PDO("mysql:host={$this->sHostname};dbname={$this->sDatabase};charset=utf8", $this->sUsername, $this->sPassword);
            $this->pdo->exec("SET NAMES 'utf8';");
            $this->pdo->exec("SET time_zone = ‘+00:00′");
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->db = new NotORM($this->pdo, NULL);
        } catch (PDOException $e) {
            error_log($e->errorInfo);
            return false;
        }

        return true;
    }

    protected function addCreatedTime(&$array) {
        $array["created"] = new NotORM_Literal("UTC_TIMESTAMP()");
        $array["modified"] = new NotORM_Literal("UTC_TIMESTAMP()");
    }

    protected function addModifiedTime(&$array) {
        $array["modified"] = new NotORM_Literal("UTC_TIMESTAMP()");
    }

    protected function addGUID(&$array) {
        if (!isset($array['id'])) {
            $array['id'] = NMHelper::guid();
        }
    }

}