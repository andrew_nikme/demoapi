<?php

/**
 * Description of NMModel
 *
 * @author NGUYENThe
 */
class NMModel {

    function __construct($dbROW = NULL) {
        if ($dbROW != NULL) {
            $array_properties = get_class_vars(get_class($this));
            $array_keys = array_keys($array_properties);
            foreach ($array_keys as $key) {
                if (isset($dbROW[$key])) {
                    $this->$key = $dbROW[$key];
                }
            }
        }
    }

}
