<?php

/**
 * Description of NMWSModel
 *
 * @author Andrew NGUYEN
 */
class NMWSModel {

    public function __construct() {
        
    }

    public function toJSon() {
        return json_encode($this);
    }

}
