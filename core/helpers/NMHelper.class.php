<?php

/**
 * Description of NMHelper
 *
 * @author Linh NGUYEN
 */
class NMHelper {

    public static function getDistanceBetweenPoints($lat1, $lon1, $lat2, $lon2) {
        $lat1Radian = deg2rad($lat1);
        $lon1Radian = deg2rad($lon1);
        $lat2Radian = deg2rad($lat2);
        $lon2Radian = deg2rad($lon2);
        $distance = acos(sin($lat1Radian) * sin($lat2Radian) + cos($lat1Radian) * cos($lat2Radian) * cos($lon1Radian - $lon2Radian)) * EARTH_RADIUS;
        // KM
        return round($distance, 3);
    }

    public static function convertDate($sql_date) {
        $date = strtotime($sql_date);
        $final_date = date("F j, Y, g:i a", $date);
        return $final_date;
    }

    public static function ago($from, $now, $rcs = 0) {
        $dt = strtotime($from);
        $cur_tm = strtotime($now);
        $suffix = "ago";
        $dif = $cur_tm - $dt;
        $pds = array('second', 'minute', 'hour', 'day', 'week', 'month', 'year', 'decade');
        $lngh = array(1, 60, 3600, 86400, 604800, 2630880, 31570560, 315705600);
        for ($v = sizeof($lngh) - 1; ($v >= 0) && (($no = $dif / $lngh[$v]) <= 1); $v--) {
            if ($v < 0) {
                $v = 0;
            }
        }
        if ($v < sizeof($lngh) && $v >= 0) {
            $_tm = $_tm - ($dif % $lngh[$v]);
        } else {
            return 'now';
        }

        $no = floor($no);
        if ($no <> 1) {
            $pds[$v] .='s';
        } $x = sprintf("%d %s ", $no, $pds[$v]);
        if (($rcs == 1) && ($v >= 1) && (($cur_tm - $_tm) > 0)) {
            $x .= time_ago($_tm);
        }
        return $x . $suffix;
    }

    public static function getUTCTimeStamp() {
        date_default_timezone_set("UTC");
        $UTCtime = date("Y-m-d H:i:s", time());
        return strtotime($UTCtime);
    }

    public static function getUTCDate() {
        date_default_timezone_set("UTC");
        $UTCdate = date("Y-m-d", time());
        return $UTCdate;
    }

    public static function getUTCTime() {
        date_default_timezone_set("UTC");
        $UTCtime = date("H:i:s", time());
        return $UTCtime;
    }

    public static function getUTCDatetime() {
        date_default_timezone_set("UTC");
        $UTCdate = date("Y-m-d H:i:s", time());
        return $UTCdate;
    }

    public static function validRequest($body, $required, $optional = NULL) {
        if (!$body) {
            $body = array();
        }

        if ($required == NULL) {
            $required = array();
        }
        if ($optional == NULL) {
            $optional = array();
        }

        foreach (array_keys($body) as $key) {
            if (!in_array($key, $required) && !in_array($key, $optional)) {
                return ErrorCodeMapping::$VALID_REQUEST_BAD;
            }
        }

        foreach ($required as $value) {
            if (!isset($body[$value])) {
                $errorArray = ErrorCodeMapping::$VALID_REQUIRED;
                $errorArray[1] = str_replace('{FIELD}', $value, $errorArray[1]);
                return $errorArray;
            }
        }

        return NULL;
    }

    public static function guid() {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    public static function sendAndroidPush($registrationIDs, $message, $apiKey) {
        $url = 'http://android.googleapis.com/gcm/send';

        $fields = array(
            'registration_ids' => $registrationIDs,
            'data' => array("text" => $message),
        );

        $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        );

        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);

        // Close connection
        curl_close($ch);

        return $result;
    }

}
