<?php

require_once './config.php';

//////////////////////////////////////////////////////////////////////
// INIT SLIM
//////////////////////////////////////////////////////////////////////
$config = array(
    'templates.path' => './app/resources/templates'
);
$app = new \Slim\Slim($config);

$app->hook('slim.before', function () use ($app) {
    $app->view()->appendData(array('baseUrl' => ROOT_PATH));
});

$app->view(new \Slim\Views\Twig());

$app->view->parserOptions = array(
    'charset' => 'utf-8',
    'cache' => realpath('./cache'),
    'auto_reload' => true,
    'strict_variables' => false,
    'autoescape' => true
);

$app->view->parserExtensions = array(new \Slim\Views\TwigExtension());

//////////////////////////////////////////////////////////////////////
// AUTHENTICATION
//////////////////////////////////////////////////////////////////////
$headers = apache_request_headers();

$app->add(new SessionAuth(array(
    'expires' => '20 minutes',
    'path' => '/',
    'domain' => null,
    'secure' => false,
    'httponly' => false,
    'name' => 'slim_session',
    'secret' => APPLICATION_SECRET_KEY,
    'cipher' => MCRYPT_RIJNDAEL_256,
    'cipher_mode' => MCRYPT_MODE_CBC
)));

//////////////////////////////////////////////////////////////////////
// CONFIG FOR SLIM
//////////////////////////////////////////////////////////////////////
$mode = DEVELOPMENT_MODE == TRUE ? "development" : "production";
$app->config("mode", $mode);

// 404 handler
$app->notFound(function () use ($app) {
    $app->render('404.twig');
});

// error handler
$app->error(function (\Exception $e) use ($app) {
    $app->view()->setData(array('error' => $e->getMessage()));
    $app->render('error.twig');
});

$rootController = new RootController($app);

//////////////////////////////////////////////////////////////////////
// DEFINE WEB
//////////////////////////////////////////////////////////////////////

$app->get('/users', function () {
    global $rootController;
    $rootController->viewAllUsers();
});

// run the Slim app
$app->run();