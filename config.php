<?php

//////////////////////////////////////////////////////////////////////
// Includes
//////////////////////////////////////////////////////////////////////
// AutoLoad
require_once 'vendor/autoload.php';

// NMCore
require_once './core/models/NMWSModel.class.php';
require_once './core/models/NMModel.class.php';
require_once './core/helpers/Image.class.php';
require_once './core/helpers/NMHelper.class.php';
require_once './core/daos/NMDAO.class.php';
require_once './core/endpoints/NMEndPoint.class.php';
require_once './core/controllers/NMController.php';

// application
require_once './app/commons/ErrorCodeMapping.class.php';
require_once './app/commons/GlobalVariables.php';
require_once './app/services/validations/OAuth2Auth.class.php';
require_once './app/services/validations/SessionAuth.class.php';
require_once './app/services/validations/RequestValidator.class.php';
require_once './app/helpers/CommonHelpers.class.php';

require_once './app/services/schemas/BaseModel.class.php';
require_once './app/services/schemas/User.class.php';

require_once './app/daos/BaseDAO.class.php';
require_once './app/daos/AuthDAO.class.php';
require_once './app/daos/UserDAO.class.php';
require_once './app/services/endpoints/APIEndpoint.class.php';

require_once './app/controllers/RootController.class.php';


//////////////////////////////////////////////////////////////////////
// INIT GET HEADER FUNCTION
//////////////////////////////////////////////////////////////////////

if (!function_exists('apache_request_headers')) {

    function apache_request_headers() {
        $arh = array();
        $rx_http = '/\AHTTP_/';
        foreach ($_SERVER as $key => $val) {
            if (preg_match($rx_http, $key)) {
                $arh_key = preg_replace($rx_http, '', $key);
                $rx_matches = array();
                $rx_matches = explode('_', $arh_key);
                if (count($rx_matches) > 0 and strlen($arh_key) > 2) {
                    foreach ($rx_matches as $ak_key => $ak_val)
                        $rx_matches[$ak_key] = ucfirst($ak_val);
                    $arh_key = implode('-', $rx_matches);
                }
                $arh[$arh_key] = $val;
            }
        }
        return( $arh );
    }

}