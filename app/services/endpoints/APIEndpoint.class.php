<?php

class APIEndpoint extends NMEndPoint {

    private $userDAO;

    public function __construct($app, $requesterId) {
        parent::__construct($app, $requesterId);
        $this->initDaos();
    }

    private function initDaos() {
        $this->userDAO = new UserDAO();
    }

    //////////////////////////////////////////////////////////////////////
    // WS METHODS
    //////////////////////////////////////////////////////////////////////

    /*
     * Social Auth
     */
    public function socialAuth() {
        $body = $this->processRequestBody();
        // validate
        if (!$body) {
            return;
        }
        if (($result = RequestValidator::socialAuth($body)) != NULL) {
            $this->returnValidateError($result);
            return;
        }
        $user = $this->userDAO->socialAuth($body);
        if ($user == NULL) {
            $this->returnError(ErrorCodeMapping::$AUTH_LOGIN_FAILED);
        } else {
            $this->returnData($user);
        }
    }
}
