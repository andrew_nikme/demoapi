<?php

/**
 * Description of User
 *
 * @author NGUYENThe
 */
class User extends BaseModel {

    public $id;
    public $name;
    public $pic;
    public $sex;
    public $open_uuid;
    public $token;
    public $created;
    public $modified;

    public function unsetForView() {
        unset($this->token);
    }

}
