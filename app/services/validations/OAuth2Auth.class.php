<?php

class OAuth2Auth extends \Slim\Middleware {

    protected $headers = array();
    protected $authDAO;
    protected $privateKey;

    public function __construct($headers, $authDAO) {
        $this->headers = $headers;
        $this->authDAO = $authDAO;
    }

    public function call() {
        if (!isset($this->headers[HEADER_APPLICATION_KEY]) || !isset($this->headers[HEADER_SIGNATURE]) || !isset($this->headers[HEADER_TIMESTAMP])) {
            $this->sayError(401, ErrorCodeMapping::$AUTH_COMMON);
        } else {
            $r_app_id = $this->headers[HEADER_APPLICATION_KEY];
            $r_timestamp = $this->headers[HEADER_TIMESTAMP];
            $r_signature = $this->headers[HEADER_SIGNATURE];

            // check valid token
            if (isset($this->headers[HEADER_UID])) {
                $uid = $this->headers[HEADER_UID];
                if (!isset($this->headers[HEADER_TOKEN])) {
                    $this->sayError(401, ErrorCodeMapping::$AUTH_DENIED);
                    return;
                } else {
                    $token = $this->headers[HEADER_TOKEN];
                    $errorArray = $this->authDAO->validToken($uid, $token);
                    if ($errorArray != NULL) {
                        $this->sayError(401, $errorArray);
                        return;
                    }
                }
            }
            // check api version
            $this->privateKey = $this->authDAO->getPrivateKey($r_app_id);
            //check timestamp
            if (NMHelper::getUTCTimeStamp() - $r_timestamp > TIMESTAMP_LIMIT) {
                $this->sayError(401, ErrorCodeMapping::$AUTH_TIME_STAMP);
            } else {
                $this->checkHMAC($r_signature, $r_timestamp, $this->privateKey);
            }
        }
    }

    private function checkHMAC($r_signature, $r_timestamp, $private_key) {
        $request = $this->app->request();
        if (strpos($request->getContentType(), 'multipart/form-data') === false) {
            $body = $request->getBody();
        } else {
            $body = 'FILE';
        }
        $url = $request->getUrl() . $request->getPath();
        $params = $request->params();
        $i = 0;
        foreach ($params as $key => $value) {
            if ($i == 0) {
                $url = $url . '?' . $key . '=' . $value;
            } else {
                $url = $url . '&' . $key . '=' . $value;
            }
            $i++;
        }
        $method = $request->getMethod();
        $string_to_sign = $method . "&" . $url . "&" . $r_timestamp . "&" . $body;
        echo $string_to_sign;
        $hmac256 = hash_hmac("sha256", $string_to_sign, $private_key);
        $signature = base64_encode($hmac256);
        echo $signature;
        if ($r_signature != $signature) {
            $this->sayError(401, ErrorCodeMapping::$AUTH_DENIED);
        } else {
            $this->next->call();
        }
    }

    private function sayError($statusCode, $errorArray) {
        $this->app->response()->header('Content-Type', 'application/json');
        $this->app->response()->status($statusCode);
        if ($errorArray && count($errorArray) == 2) {
            $this->app->response()->header(ERROR_CODE_TAG, $errorArray[0]);
            $this->app->response()->header(ERROR_MESSAGE_TAG, $errorArray[1]);
        }
    }

}
