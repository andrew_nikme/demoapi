<?php

/**
 * Description of RequestValidator
 *
 * @author Linh NGUYEN
 */
class RequestValidator {

    public static function socialAuth($body) {
        $required = array('open_uuid', 'name', 'device_uuid');
        $optional = array('pic', 'sex', 'device_token');
        return NMHelper::validRequest($body, $required, $optional);
    }
}
