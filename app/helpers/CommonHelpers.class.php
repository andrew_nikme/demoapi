<?php

/**
 *
 * @author Linh NGUYEN
 */
class CommonHelpers {

    public static function sendEmail($to, $subject, $body) {
        $headers = "From: " . strip_tags("donotreply@nikmesoft.com") . "\r\n";
        $headers .= "Reply-To: " . strip_tags($to) . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        if (mail($to, $subject, $body, $headers)) {
            return NULL;
        } else {
            return ErrorCodeMapping::$EMAIL_DELIVERY_FAILED;
        }
    }

    public static function generatePassword($length = 8) {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

}
