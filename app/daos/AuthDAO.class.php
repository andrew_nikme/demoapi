<?php

/**
 * Description of AuthDAO
 *
 * @author NGUYENThe
 */
class AuthDAO extends BaseDAO {

    public function getPrivateKey($application_id) {
        $appRow = $this->db->application("id = ?", $application_id)->fetch();
        if ($appRow != NULL) {
            return $appRow["private_key"];
        }
        return NULL;
    }

    public function validToken($user_id, $token) {
        $userRow = $this->db->user_session("user_id = ? AND token = ?", $user_id, $token)->fetch();
        if ($userRow == NULL) {
            return ErrorCodeMapping::$AUTH_SESSION_EXPIRED;
        } else {
            $expDate = $userRow['expiration_date'];
            if (strtotime($expDate) < strtotime(NMHelper::getUTCDate())) {
                return ErrorCodeMapping::$AUTH_SESSION_EXPIRED;
            }
        }
        return NULL;
    }

}