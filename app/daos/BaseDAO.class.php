<?php

/**
 * Description of BaseDAO
 *
 * @author linhnguyen
 */
class BaseDAO extends NMDAO {

    public function __construct() {
        parent::__construct(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_NAME);
    }

    protected function log($obj) {
        \Slim\Slim::getInstance() . log($obj);
    }

}
