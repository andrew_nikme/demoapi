<?php

/**
 *
 * @author Linh NGUYEN
 */
class UserDAO extends BaseDAO {

    private function removeSession($user_id, $device_uuid) {
        $this->db->user_session('user_id = ? AND device_uuid = ?', $user_id, $device_uuid)->delete();
    }

    private function addSession($user_id, $device_uuid, $device_token) {
        $array = array('user_id' => $user_id, 'device_uuid' => $device_uuid, 'device_token' => $device_token);
        $todayTimestamp = strtotime(NMHelper::getUTCDate());
        $token = md5($user_id . '-' . $device_uuid . '-' . $todayTimestamp);
        $expiration_date = $todayTimestamp + SESSION_TIMEOUT;
        $array['token'] = $token;
        $array['expiration_date'] = date("Y-m-d", $expiration_date);
        try {
            $this->addCreatedTime($array);
            $this->db->user_session()->insert($array);
            return $token;
        } catch (PDOException $ex) {
            $ex->getTrace();
        }
    }

    public function socialAuth($array) {
        $row = $this->db->user()
                        ->where('open_uuid', $array['open_uuid'])->fetch();

        $device_uuid = $array['device_uuid'];
        $device_token = NULL;
        if (isset($array['device_token'])) {
            $device_token = $array['device_token'];
            unset($array['device_token']);
        }
        unset($array['device_uuid']);

        if ($row == NULL) {
            try {
                $this->addCreatedTime($array);
                $this->addGUID($array);
                $row = $this->db->user()->insert($array);
                $row = $this->db->user("id = ?", $row['id'])->fetch();
            } catch (PDOException $ex) {
                return NULL;
            }
        } else {
            try {
                $this->addModifiedTime($array);
                $row->update($array);
                $row = $this->db->user("id = ?", $row['id'])->fetch();
            } catch (PDOException $ex) {
                return NULL;
            }
        }

        $user = new User($row);
        $this->removeSession($user->id, $device_uuid);
        $token = $this->addSession($user->id, $device_uuid, $device_token);
        $user->token = $token;
        return $user;
    }

    public function getAllUsers() {
        $table = $this->db->user();
        $rows = $table->order("modified DESC");
        $users = array();
        foreach ($rows as $value) {
            $user = new User($value);
            $user->unsetForView();
            $users[] = $user;
        }
        return $users;
    }

}
