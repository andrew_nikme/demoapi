<?php

class ErrorCodeMapping {

    //Auth
    public static $AUTH_COMMON = array('ACCESS_CREDENTIALS_NOT_SUPPLIED', "Access credentials not supplied.");
    public static $AUTH_API_KEY = array('INVALID_API_KEY', "The API key is invalid.");
    public static $AUTH_TIME_STAMP = array('INVALID_TIMESTAMP', "The security timestamp is invalid.");
    public static $AUTH_DENIED = array('AUTH_DENIED', "Access is denied.");
    public static $AUTH_LOGIN_FAILED = array('AUTHORIZATION_FAILED', "The username or password you entered is incorrect.");
    public static $AUTH_SESSION_EXPIRED = array('SESSION_EXPIRED', "Your session has expired. Please log in again.");
    //Valid
    public static $VALID_REQUEST_BAD = array('BAD_REQUEST', "Request is invalid.");
    public static $VALID_REQUEST_TIME = array('REQUEST_TIME_LIMIT', "Please waiting 15 minutes between 2 requests.");
    public static $VALID_FILE_BAD = array('INVALID_FILE_FORMAT', "File is invalid.");
    public static $VALID_CURRENT_PASSWORD = array('OLD_PASSWORD_WRONG', "Unable to change password [Old password is incorrect]");
    // Not found
    public static $VALID_METHOD_NOT_FOUND = array('METHOD_NOT_FOUND', "The request resource is not found.");
    public static $VALID_REQUIRED = array('MISSING_REQUIRED_PARAMS', "{FIELD} is required.");
    //Exist
    public static $EXIST_EMAIL = array('EMAIL_IS_EXIST', "This email address is already in use.");
    //Not exist
    public static $NOT_EXIST_USER_ID = array('USER_ID_NOT_EXIST', "User ID does not exist.");
    public static $NOT_EXIST_EMAIL = array('EMAIL_NOT_EXIST', "Email does not exist.");
    //Send Email Failed
    public static $EMAIL_DELIVERY_FAILED = array('EMAIL_DELIVERY_FAILED', "Email delivery failed.");

}
