<?php

/**
 * Description of GlobalVariables
 *
 * @author Linh NGUYEN
 */
// TIME FORMAT
define('DATE_TIME_FORMAT', "Y-m-d H:i:s");

// Database info
define('MYSQL_HOST', 'localhost');
define('MYSQL_USER', 'thelinh_admin');
define('MYSQL_PASS', 'abcde12345-');
define('MYSQL_NAME', 'thelinh_demo');

// AUTHORIZE
define("TIMESTAMP_LIMIT", 30 * 60 * 60 * 24);
define("SESSION_TIMEOUT", 7 * 86400);
define("ENCODED_BODY", FALSE);
define("DEVELOPMENT_MODE", TRUE);

// REQUEST TIME
define("REQUEST_TIME", 900);

//PATH
define("ROOT_PATH", "http://nikmesoft.com/apis/DemoAPI/");
define("AVATAR_FULL_PATH", getcwd() . '/app/resources/images/avatars/');
define("AVATAR_PATH", 'app/resources/images/avatars/');
define("AVATAR_SIZE_LIMIT", 10 * 1024 * 1024);

// HEADERS
define('ERROR_CODE_TAG', 'nm-code');
define('ERROR_MESSAGE_TAG', 'nm-message');

// HEADERS VALUE
define('HEADER_TIMESTAMP', 'TIMESTAMP');
define('HEADER_APPLICATION_KEY', 'APPLICATION-KEY');
define('HEADER_SIGNATURE', 'SIGNATURE');
define('HEADER_UID', 'NM-UID');
define('HEADER_TOKEN', 'NM-TOKEN');
