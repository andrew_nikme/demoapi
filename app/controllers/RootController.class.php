<?php

class RootController extends NMController {

    private $userDAO;

    public function __construct($app) {
        parent::__construct($app);
        $this->initDAOs();
    }

    private function initDAOs() {
        $this->userDAO = new UserDAO();
    }

    //////////////////////////////////////////////////////////////////////
    // WEB PAGES DEFINE
    //////////////////////////////////////////////////////////////////////
    private function setPageTitle($title) {
        $this->app->view()->setData(array('title' => $title));
    }

    public function viewAllUsers() {
        $users = $this->userDAO->getAllUsers();
        $this->setPageTitle('Users');
        $this->app->view()->setData(array('users' => $users));
        $this->app->render('users.list.twig');
    }

}
